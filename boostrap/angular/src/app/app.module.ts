import { InicioComponent } from './inicio/inicio.component';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppComponent } from './app.component';
import { RutaNoEncontrada } from './autor/ruta-no-encontrada/ruta-no-encontrada.component';
import { RutasModule } from './app.routes';

@NgModule({
  declarations: [
    AppComponent,
    InicioComponent,
    RutaNoEncontrada,
  ],
  imports: [
    BrowserModule,
    RutasModule,
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
