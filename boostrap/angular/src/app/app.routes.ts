import { InicioComponent } from './inicio/inicio.component';
import { Route, RouterModule } from "@angular/router";
import { RutaNoEncontrada } from "./autor/ruta-no-encontrada/ruta-no-encontrada.component";
import { NgModule } from "@angular/core";
const rutas: Route[] = [
    {
        path: 'inicio',
        component: InicioComponent
    },
    {
        path: 'autor',
        loadChildren: 'src/app/autor/autor.module#AutorModule',
    },
    {
        path: '',
        redirectTo: 'inicio',
        pathMatch: 'full'
    },
    {
        path: '**',
        component: RutaNoEncontrada
    },
]
@NgModule({
    imports: [RouterModule.forRoot(rutas, { useHash: true })],
    exports: [RouterModule]
})
export class RutasModule {}