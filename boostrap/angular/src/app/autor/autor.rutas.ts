import { NgModule } from "@angular/core";
import { Route, RouterModule } from "@angular/router";
import { AutorBiografiaComponent } from "./autor-biografia/autor-biografia.component";
import { InfoAutorComponent } from "./info-autor/autor.component";
import { CrearAutorComponent } from "./crear-autor/crear-autor.component";
import { AutorListarComponent } from "./autor-listar/autor-listar.component";
const rutasAutor: Route[]=[
    {
        path: 'biografia/:idAutor',
        component: AutorBiografiaComponent,
        outlet: 'autor',
    },
    {
        path: 'info',
        component: InfoAutorComponent,
        outlet: 'autor',
    },
    {
        path: 'crear',
        component: CrearAutorComponent,
        outlet: 'autor',
    },
    {
        path: 'listar',
        component: AutorListarComponent,
        outlet: 'autor',
    },
    {
        path: '',
        redirectTo: 'info',
        pathMatch: 'full',
    },
]
@NgModule({
    declarations:[],
    imports:[RouterModule.forChild(rutasAutor)],
    exports:[RouterModule],
})
export class RutasAutorModule{}