import { NgModule } from "@angular/core";
import { RutasAutorModule } from "./autor.rutas";
import { CrearAutorComponent } from "./crear-autor/crear-autor.component";
import { AutorBiografiaComponent } from "./autor-biografia/autor-biografia.component";
import { InfoAutorComponent } from "./info-autor/autor.component";
import { AutorListarComponent } from "./autor-listar/autor-listar.component";
@NgModule({
    declarations: [
        AutorBiografiaComponent,
        CrearAutorComponent,
        InfoAutorComponent,
        AutorListarComponent,
    ],
    imports: [
        RutasAutorModule
    ],
    exports: [],
    providers: []
})
export class AutorModule { }