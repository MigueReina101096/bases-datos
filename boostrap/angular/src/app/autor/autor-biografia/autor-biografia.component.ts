import { Component, OnInit } from "@angular/core";
import { ActivatedRoute } from "@angular/router";
@Component({
    selector: 'autor-biografia',
    templateUrl: './autor-biografia.component.html',
    styleUrls:['./autor-biografia.component.css'],
})
export class AutorBiografiaComponent implements OnInit {
    constructor(private readonly _activatedRoute : ActivatedRoute){
        const parametros$ = this._activatedRoute.paramMap;
        parametros$.subscribe((respuestaParam)=>{
            console.log(respuestaParam.get('idAutor'));
        })
        const queryParam$ = this._activatedRoute.queryParamMap;
        queryParam$.subscribe((respuestaQuery)=>{
            console.log(respuestaQuery.get('nombre'));
        })
    }
    ngOnInit(){
        console.log('Esta iniciando autor biografia component.');
    }
}