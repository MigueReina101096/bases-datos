import {Entity, PrimaryGeneratedColumn, Column, BaseEntity, OneToMany} from "typeorm";
import { ArtistaDto } from "./dto/artistas.dto";
import { CancionEntity } from "cancion/cancion.entity";
import { NombreEntity } from "common/entities/nombre.entity";

@Entity('artista_entity')
export class ArtistaEntity {

    @PrimaryGeneratedColumn()
    id: number;

    @Column(type => NombreEntity)
    artista: NombreEntity;

    @Column({type: 'varchar', name: 'mc_password', default: '1234'})
    password: string;

    @Column({type: 'enum', name: 'estado', enum: ['activo', 'retirado']})
    estado: string;

    @OneToMany(type => CancionEntity, cancion => cancion.artista)
    canciones: CancionEntity[]
}