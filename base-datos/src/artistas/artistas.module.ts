import { ArtistaController } from './artista.controller';
import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { ArtistaEntity } from './artistas.entity';
import { ArtistaResolver } from './artistas.resolver';
import { ArtistaService } from './artistas.service';

@Module({
    imports: [
        TypeOrmModule.forFeature([
            ArtistaEntity
        ],'conexion')
    ],
    controllers: [ArtistaController],
    providers: [ ArtistaResolver, ArtistaService]
})

export class ArtistaModule {}

