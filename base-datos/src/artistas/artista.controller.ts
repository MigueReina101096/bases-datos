import { ArtistaDto } from './dto/artistas.dto';
import { Controller, Get, Post, Body, Param } from "@nestjs/common";
import { ArtistaService } from "./artistas.service";

@Controller('artista')
export class ArtistaController {
    constructor(private readonly _artistaService: ArtistaService) { }
    @Get()
    obtenerTodos() {
        return this._artistaService.buscarTodos()
    }
    @Get('buscar/:id')
    obtenerUno(
        @Param() idArtista
    ) {
        return this._artistaService.buscarUno(idArtista)
    }
    @Post('crear')
    crearArtista(
        @Body() artistaDto: ArtistaDto
    ) {
        return this._artistaService.crear(artistaDto)
    }
    @Post('crearUno')
    crearVariosArtista(
        @Body() artistaDto: ArtistaDto
    ) {
        return this._artistaService.crearVarios(artistaDto)
    }
    @Post('eliminar/:id')
    eliminaruno(
        @Param() idArtista
    ){
        return this._artistaService.eliminar(idArtista)
    }
}