import { ArtistaEntity } from './artistas.entity';
import { Injectable } from "@nestjs/common";
import { InjectRepository } from "@nestjs/typeorm";
import { Repository } from 'typeorm';

@Injectable()
export class ArtistaService {

    constructor(
        @InjectRepository(ArtistaEntity)
        private readonly _artistaRepository: Repository<ArtistaEntity>,
      ) {}

    async buscarTodos():Promise<any>{
        return await this._artistaRepository.find()
    }
    async buscarUno(id: number):Promise<any>{
        return await this._artistaRepository.findOne(id)
    }
    async crear(artista):Promise<any>{
        const variable = this._artistaRepository.create(artista)
        return await this._artistaRepository.save(variable)
    }

    async crearVarios(artista):Promise<any>{
        return await this._artistaRepository.create(artista)
    }
    async eliminar(id: number):Promise<any>{
        return await this._artistaRepository.delete(id)
    }
    async actualizar(id):Promise<any>{
        
    } 
}