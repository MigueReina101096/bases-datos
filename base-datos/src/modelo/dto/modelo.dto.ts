import { IsInt, IsOptional, IsAlpha } from 'class-validator';
export class ModeloDto{
    @IsInt()
    @IsOptional()
    id?: number
    @IsAlpha()
    modelo: string
}