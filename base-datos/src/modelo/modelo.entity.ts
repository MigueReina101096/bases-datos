import { Entity, PrimaryGeneratedColumn, Column } from "typeorm";

@Entity()
export class ModeloEntity {
    
    @PrimaryGeneratedColumn()
    id: number;

    @Column({type: 'varchar', name: 'marca'})
    modelo: string;
}