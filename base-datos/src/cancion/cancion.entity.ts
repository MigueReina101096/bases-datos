import { Entity, PrimaryGeneratedColumn, Column, ManyToOne } from "typeorm";
import { ArtistaEntity } from "artistas/artistas.entity";
import { NombreEntity } from "common/entities/nombre.entity";

@Entity()
export class CancionEntity {
    @PrimaryGeneratedColumn()
    id: number;

    @Column(type => NombreEntity)
    cancion: NombreEntity

    @ManyToOne(type => ArtistaEntity, artista => artista.canciones)
    artista: ArtistaEntity
}