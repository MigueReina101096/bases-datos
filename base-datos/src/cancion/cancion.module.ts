import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { CancionEntity } from './cancion.entity';


@Module({
    imports: [
        TypeOrmModule.forFeature([
            CancionEntity
        ])
    ],
    controllers: [],
    providers: []
})

export class CancionModule {}

