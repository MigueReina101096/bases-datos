import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Connection } from 'typeorm';
import { ArtistaModule } from 'artistas/artistas.module';
import { AutoModule } from 'auto/auto.module';
import { ArtistaEntity } from 'artistas/artistas.entity';
import { AutoEntity } from 'auto/auto.entity';
import { CancionEntity } from 'cancion/cancion.entity';
import { BusEntity } from 'bus/bus.entity';
import { BusModule } from 'bus/bus.module';
import { WebsocketRedisModule } from 'websocket-redis/websocket-redis.module';

@Module({
  imports: [
    // TypeOrmModule.forRoot({
    //   type: 'mysql',
    //   host: '192.168.100.126',
    //   name: 'conexion2',
    //   port: 32769,
    //   username: 'manticore',
    //   password: '12345678',
    //   database: 'transporte',
    //   // dropSchema: true,
    //   entities: [BusEntity],
    //   synchronize: false,
    // }),
    // TypeOrmModule.forRoot({
    //   type: 'mysql',
    //   host: 'localhost',
    //   name: 'conexion2',
    //   port: 32771,
    //   username: 'miguel',
    //   password: 'root',
    //   database: 'auto',
    //   dropSchema: true,
    //   entities: [AutoEntity],
    //   synchronize: true,
    // }),
    // ArtistaModule,
    // AutoModule,
    // BusModule,
    WebsocketRedisModule,
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {
  constructor() {
    // this.iniciar();
  }
  iniciar(){
    console.log('va a iniciar la creacion de mis datos de prueba')
  }
};
