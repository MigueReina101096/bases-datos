import { Entity, PrimaryGeneratedColumn, Column, ManyToOne } from "typeorm";
import { EscritorEntity } from "escritor/escritor.entity";

@Entity()
export class LibroEntity {

    @PrimaryGeneratedColumn()
    id: number;

    @Column({type: 'varchar', name:'l_nombre', length: 100})
    nombre: string;
    
    @Column({type: 'varchar', name: 'l_editorial', length: 150})
    editorial: string;

    @Column({type: 'int', name: 'l_año-publicacion'})
    añoPublicacion: number;

    @Column({type: 'enum', name: 'l_idioma', enum: ['inglés', 'español', 'francés', 'chino', 'italiano']})
    idioma: string;

    @Column({type: 'enum', name: 'l_genero_literario', enum:['fantasía', 'horror', 'ciencia-ficción', 'policial', 'historicas']})
    generoLiterario: string;

    @Column({type: 'varchar', name: 'l_traducciones', default: 'Si'})
    traducciones: string;

    @Column({type: 'float', name: 'l_precio'})
    precio: number;

    @ManyToOne(type => EscritorEntity, escritor => escritor.libros)
    escritor: EscritorEntity

}