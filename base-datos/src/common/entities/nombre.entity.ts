import { Entity, Column, PrimaryGeneratedColumn } from "typeorm";

export class NombreEntity {

    @Column({type: 'varchar', name:'mc_nombre', length: 500})
    nombre: string;  
}