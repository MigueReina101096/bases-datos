import { Injectable, BadRequestException } from "@nestjs/common";
import { InjectRepository } from "@nestjs/typeorm";
import { Repository } from "typeorm";
import { AutoEntity } from "./auto.entity";
import { from, Observable } from "rxjs";

@Injectable()

export class AutoService {

    constructor(@InjectRepository(AutoEntity) private readonly autoRepository: Repository<AutoEntity>) { }

    obtenerTodosObservable(skip: number, limit: number): Observable<AutoEntity[]> {
        return from(this.obtenerTodos(skip, limit))
    }

    async obtenerTodos(skip: number, limit: number): Promise<AutoEntity[]> {
        return await this.autoRepository.find({
            order: {
                id: 'ASC'
            },
            skip: skip,
            take: limit
        })
    }

    obtenerPorIdObservable(id: number): Observable<AutoEntity> {
        return from(this.obternPorId(id))
    }

    obternPorId(id: number): Promise<AutoEntity> {
        return this.autoRepository.findOne(id)
    }

    eliminarObservable(id: number): Observable<AutoEntity> {
        return from(this.eliminar(id))
    }

    async eliminar(id: number): Promise<any> {
        return await this.autoRepository.delete(id)
    }
    async actualizar(id: number, autoActualizado: AutoEntity): Promise<any> {
        const autoEncontrado = await this.obternPorId(id)
        if (autoEncontrado) {
            const autoVerificado = actualizarAuto(autoEncontrado, autoActualizado)
            return this.autoRepository.update(id, autoVerificado)
        } else {
            throw new BadRequestException({ mensaje: 'No se encontro el auto, no se actualizo' })
        }
    }
    crear(nuevoAuto: AutoEntity): Promise<AutoEntity> {
        return this.autoRepository.save(nuevoAuto)
    }
    crearVarios(nuevosAutos: AutoEntity[]): Promise<AutoEntity[]> {
        const autosGuardados: AutoEntity[] = this.autoRepository.create(nuevosAutos)
        return this.autoRepository.save(autosGuardados)
    }
    query(skip: number, limit: number, consulta: object): Promise<AutoEntity[]> {

        return this.autoRepository.find({
            order: {
                id: 'DESC'
            },
            skip: skip,
            take: limit,
            where: consulta
        })
    }
    contar(consulta: object) {
        return this.autoRepository.count({
            where: consulta
        })
    }

}




function actualizarAuto(auto: AutoEntity, autoActualizado): AutoEntity {
    let autoModificado
    Object.getOwnPropertyNames(auto).forEach(atributo => {
        let tieneValorDiferente = auto[atributo] != autoActualizado[atributo]
        if (tieneValorDiferente) {
            autoModificado[atributo] = autoActualizado[atributo]
        } else {
            autoModificado[atributo] = auto[atributo]
        }
    })
    return autoModificado

}