import { Entity, PrimaryGeneratedColumn, Column } from "typeorm";

@Entity()
export class AutoEntity {
    
    @PrimaryGeneratedColumn()
    id: number;

    @Column({type: 'varchar', name: 'marca'})
    marca: string;

    @Column({type: 'varchar', name: 'color'})
    color: string;

    @Column({type: 'varchar', name: 'placa'})
    estado: string;
}