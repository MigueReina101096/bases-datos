import { IsInt, IsOptional, IsAlpha, IsAlphanumeric } from 'class-validator';
export class AutoDto {
    @IsInt()
    @IsOptional()
    id?: number
    @IsAlpha()
    marca: string
    @IsAlpha()
    color: string
    @IsAlphanumeric()
    placa: string
}