import { Controller, Get, Post, Param, Query } from "@nestjs/common";
import { AutoService } from "./auto.service";

@Controller('auto')
export class AutoController {
    constructor(
        private readonly _autoService: AutoService
    ) { }
    @Get('obtenerTodos')
    obtenerTodos(
        @Query('skip') skip,
        @Query('limit') limit,
    ) {
        return this._autoService.obtenerTodosObservable(skip, limit)
    }
    @Get('obtenerUno')
    obtenerUno(
        @Query('id') id: number,
    ) {
        return this._autoService.obtenerPorIdObservable(id)
    }
    @Post('eliminar/:id')
    eliminar(
        @Param() id: number,
    ) {
        return this._autoService.eliminar(id)
    }
}