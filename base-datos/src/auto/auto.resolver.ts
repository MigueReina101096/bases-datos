import { Controller } from "@nestjs/common";
import { AutoService } from "./auto.service";

@Controller()
export class AutoResolver {
    constructor (private readonly _autoService: AutoService) { }
}