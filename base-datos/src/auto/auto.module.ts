import { Module } from "@nestjs/common";
import { AutoController } from "./auto.controller";
import { AutoResolver } from "./auto.resolver";
import { AutoService } from "./auto.service";
import { TypeOrmModule } from "@nestjs/typeorm";
import { AutoEntity } from "./auto.entity";

@Module({
    imports: [TypeOrmModule.forFeature([
        AutoEntity
    ],'conexion1')],
    controllers:[AutoController],
    providers: [AutoResolver,AutoService],
})
export class AutoModule {}