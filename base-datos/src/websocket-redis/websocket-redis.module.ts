import { Module } from "@nestjs/common";
import { UsuariosGateway } from "./usuarios/usuarios-gateway";
import { OperadoraGateway } from "./operadora-escucha/operadora-escucha-gateway";

@Module({
    providers: [UsuariosGateway, OperadoraGateway],
    exports: [OperadoraGateway /* UsuariosGateway */ ]
})
export class WebsocketRedisModule { }