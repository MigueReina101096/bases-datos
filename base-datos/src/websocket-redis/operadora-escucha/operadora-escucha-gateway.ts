import {
    WebSocketGateway,
    WebSocketServer,
    SubscribeMessage,
    WsResponse,
    OnGatewayInit,
    OnGatewayConnection,
    OnGatewayDisconnect
} from '@nestjs/websockets';
import { from, Observable } from 'rxjs';
import { map } from 'rxjs/operators';
const io = require('socket.io-client');

@WebSocketGateway(3002)
export class OperadoraGateway implements OnGatewayInit, OnGatewayConnection, OnGatewayDisconnect {
    afterInit(server: any) {
        console.log('Init Universidades');

    }
    handleConnection(client: any, ...args: any[]) {
        console.log('Universwidad: conexion de cliente', client.id, args);
    }
    handleDisconnect(client: any) {
        console.log('disconnect', client.id);
    }
    @WebSocketServer() server;

    socket = io('http://localhost:3002/');


    @SubscribeMessage('holauno')
    holaUno(client, data): Observable<WsResponse<number>> {
        console.log('Entro a holauno');
        client.broadcast.emit('holauno', data) // los sockets que escuchan 'events'
        return data;
        // la peticion
    }

    @SubscribeMessage('holaUsuario')
    holaUsuario(client, data): Observable<WsResponse<number>> {
        console.log('Hola humano');
        client.broadcast.emit('holaUsuario', data) // los sockets que escuchan 'events'
        return data;
        // la peticion
    }

}