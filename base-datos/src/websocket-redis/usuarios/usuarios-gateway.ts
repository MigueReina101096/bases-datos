import {
    WebSocketGateway,
    WebSocketServer,
    SubscribeMessage,
    WsResponse,
    OnGatewayInit,
    OnGatewayConnection,
    OnGatewayDisconnect
} from '@nestjs/websockets';
import { from, Observable } from 'rxjs';
import { map } from 'rxjs/operators';
const io = require('socket.io-client');

@WebSocketGateway(3002)
export class UsuariosGateway implements OnGatewayInit, OnGatewayConnection, OnGatewayDisconnect {

    afterInit(server: any) {
        console.log('Init Usuarios');
        console.log('Entrando a 3002/uno')
    }
    handleConnection(client: any, ...args: any[]) {
        console.log('Usuario: conexion de cliente', client.id, args);
    }
    handleDisconnect(client: any) {
        console.log('disconnect', client.id);
    }
    @SubscribeMessage('holados')
    holaDos(client, data): Observable<WsResponse<number>> {
        console.log('Entro a holados');
        client.broadcast.emit('holados', data) // los sockets que escuchan 'events'
        return data;
        // la peticion
    }

    @WebSocketServer() server;
    socket;
}