import { Controller, Get, Post, Param, Query } from "@nestjs/common";
import { BusService } from "./bus.service";

@Controller('bus')
export class BusController {
    constructor(
        private readonly _busService: BusService
    ) { }
    @Get('obtenerTodos')
    obtenerTodos(
        @Query('skip') skip,
        @Query('limit') limit,
    ) {
        return this._busService.obtenerTodosObservable(skip, limit)
    }
}