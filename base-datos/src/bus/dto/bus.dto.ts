import { IsInt, IsOptional, IsAlpha, IsAlphanumeric } from 'class-validator';
export class AutoDto {
    @IsInt()
    @IsOptional()
    id?: number
    @IsAlpha()
    bus_nombre: string
    @IsInt()
    bus_capacidad: number 
    @IsAlphanumeric()
    bus_placa: string
}