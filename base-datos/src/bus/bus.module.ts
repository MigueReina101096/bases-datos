import { Module } from "@nestjs/common";
import { TypeOrmModule } from "@nestjs/typeorm";
import { BusEntity } from "./bus.entity";
import { BusController } from "./bus.controller";
import { BusService } from "./bus.service";
import { BusResolver } from "./bus.resolver";


@Module({
    imports: [TypeOrmModule.forFeature([
        BusEntity
    ],'conexion2')],
    controllers:[BusController],
    providers: [BusResolver,BusService],
})
export class BusModule {}