import { Controller } from "@nestjs/common";
import { BusService } from "./bus.service";

@Controller()
export class BusResolver {
    constructor (private readonly _busService: BusService) { }
}