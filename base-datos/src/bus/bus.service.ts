import { Injectable, BadRequestException } from "@nestjs/common";
import { InjectRepository } from "@nestjs/typeorm";
import { Repository } from "typeorm";
import { BusEntity } from "./bus.entity";
import { from, Observable } from "rxjs";

@Injectable()

export class BusService {

    constructor(@InjectRepository(BusEntity) private readonly autoRepository: Repository<BusEntity>) { }

    obtenerTodosObservable(skip: number, limit: number): Observable<BusEntity[]> {
        return from(this.obtenerTodos(skip, limit))
    }

    async obtenerTodos(skip: number, limit: number): Promise<BusEntity[]> {
        return await this.autoRepository.find({
            order: {
                id: 'ASC'
            },
            skip: skip,
            take: limit
        })
    }
}