import { Entity, PrimaryGeneratedColumn, Column } from "typeorm";

@Entity()
export class BusEntity {
    
    @PrimaryGeneratedColumn()
    id: number;

    @Column({type: 'varchar', name: 'bus_nombre'})
    bus_nombre: string;

    @Column({type: 'int', name: 'bus_capacidad'})
    bus_capacidad: number;

    @Column({type: 'varchar', name: 'bus_placa'})
    bus_placa: string;
}