import { Module } from "@nestjs/common";
import { TypeOrmModule } from "@nestjs/typeorm";
import { EscritorEntity } from "./escritor.entity";


@Module({
    imports: [
        TypeOrmModule.forFeature([
            EscritorEntity
        ])
    ],
    controllers: [],
    providers: []
})

export class EscritorModule {}