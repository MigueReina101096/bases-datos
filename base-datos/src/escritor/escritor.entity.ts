import { Entity, PrimaryGeneratedColumn, Column, OneToMany } from "typeorm";
import { LibroEntity } from "libro/libro.entity";

@Entity()
export class EscritorEntity {

    @PrimaryGeneratedColumn()
    id: number;

    @Column({type: 'varchar', name: 'e_nombre', length: 30})
    nombre: string;

    @Column({type: 'varchar', name: 'e_apellido', length: 30})
    apellido: string;

    @Column({type: 'varchar', name: 'e_ocupacion'})
    ocupacion: string[];

    @Column({type: 'int', name: 'e_edad'})
    edad: number;

    @Column({type: 'varchar', name: 'e_premios', default: 'No'})
    premios: string;
    
    @OneToMany(type => LibroEntity, libros => libros.escritor)
    libros: LibroEntity[]
}