import {
    WebSocketGateway,
    WebSocketServer,
    SubscribeMessage,
    WsResponse,
    OnGatewayInit,
    OnGatewayConnection,
    OnGatewayDisconnect
} from '@nestjs/websockets';
import { from, Observable } from 'rxjs';
import { map } from 'rxjs/operators';
const io = require('socket.io-client');
@WebSocketGateway(3002,{namespace:'/operadora'})
export class OperadoraGateway implements OnGatewayInit, OnGatewayConnection, OnGatewayDisconnect {
    afterInit(server: any) {
        console.log('Init Operadora-escucha');
    }
    handleConnection(client: any, ...args: any[]) {
        console.log('Operadora -> conexion de cliente', client.id, args);
    }
    handleDisconnect(client: any) {
        console.log('se desconecto el cliente: ', client.id);
    }
    // @SubscribeMessage('peticionOperadora')
    // holaUsuario(client, data): Observable<WsResponse<number>> {
    //     console.log('Entro a operadora gateway', client.id);
    //     client.broadcast.emit('respuestaUsuario', data)
    //     return data; // la peticion
    // }
    @SubscribeMessage('saludarOperadora')
    holaOperadora(cliente, data): Observable<WsResponse<number>> {
        console.log('Entro a operadora gateway', cliente.id);
        cliente.broadcast.emit('estoEsMiBroadcast', data)
        return data; // la peticion
    }
}