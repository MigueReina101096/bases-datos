import {
    WebSocketGateway,
    WebSocketServer,
    SubscribeMessage,
    WsResponse,
    OnGatewayInit,
    OnGatewayConnection,
    OnGatewayDisconnect
} from '@nestjs/websockets';
import { from, Observable } from 'rxjs';
import { map } from 'rxjs/operators';
const io = require('socket.io-client');
@WebSocketGateway(3002,{namespace:'/cliente'})
export class ClienteGateway implements OnGatewayInit, OnGatewayConnection, OnGatewayDisconnect {
    afterInit(server: any) {
        console.log('Init Cliente');
    }
    handleConnection(client: any, ...args: any[]) {
        console.log('Cliente -> conexion de operadora', client.id, args);
    }
    handleDisconnect(client: any) {
        console.log('Se desconecto el cliente: ', client.id);
    }

    @SubscribeMessage('peticionUsuario')
    holaUsuario(cliente, data): Observable<WsResponse<number>> {
        console.log('Entro a cliente gateway', cliente.id);
        cliente.broadcast.emit('respuestaUsuarioBroadcast', data)
        return data; // la peticion
    }
    @SubscribeMessage('mensajeUsuario')
    primerMensaje(cliente1, data): Observable<WsResponse<number>>{
        console.log('Este usuario envio el mensaje', cliente1.id);
        cliente1.broadcast.emit('respuestaMensajeBroadcast', data)
        return data;
    }
}